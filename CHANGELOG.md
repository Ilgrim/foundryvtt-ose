# Changelog
All notable changes to this project will be documented in this file.

## [1.0.4] - 2020-09-04
### Added
### Changed
- Fix monster saving throws, issue #135
### Removed

## [1.0.5] - 2020-09-04
### Added
### Changed
- Fix polyglot editor not initializing, [issue #41 from PolyGlot](https://github.com/kakaroto/fvtt-module-polyglot/issues/41#issuecomment-686964145)
### Removed

## [1.0.5] - 2020-09-04
### Added
- Deal XP now allow preview and modifying values
### Changed
- Fix dropping treasure table on monster from compendium
### Removed

## [1.0.7] - 2020-09-11
### Added
### Changed
- Fix individual initiative reroll
### Removed

## [1.0.8] - 2020-09-11
### Added
- Combat Tracker: Spell and Move in Combat announcement toggles
- Combat Tracker: Set Active context menu options
### Changed
- FIX tweaks ac bonus not applied with ascending AC, issue #138
### Removed

## [1.0.9] - 2020-10-04
### Added
### Changed
- Hp roll no longer add nested hp balues, fixing issue #141
- Added basic compatibility with 0.7.3
### Removed